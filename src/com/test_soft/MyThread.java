package com.test_soft;

public class MyThread extends Thread {
    private final String name;

    MyThread(String name) {
        this.name = name;
    }

    public void run() {
        try {
            HttpManager http = new HttpManager();

            switch (this.name) {
                case "get" -> http.requestGet();
                case "post" -> http.requestPost();
                case "head" -> http.requestHead();
                case "options" -> http.requestOptions();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

//        System.out.println("Executarea thread-ului `" + name + "` a luat sfarsit");
    }
}
