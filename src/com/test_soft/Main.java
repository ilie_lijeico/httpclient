package com.test_soft;

public class Main {

    public static void main(String[] args) throws Exception {
        MyThread getThread = new MyThread("get");
        MyThread postThread = new MyThread("post");
        MyThread headThread = new MyThread("head");
        MyThread optionsThread = new MyThread("options");

        getThread.start();
        postThread.start();
        headThread.start();
        optionsThread.start();
    }
}
